from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView

from students.forms import GroupForm, StudentForm, TeacherForm, FeedbackForm
from students.models import Group, Teacher
import logging

logger = logging.getLogger("django")


class HomeView(View):
    def dispatch(self, request, *args, **kwargs):
        groups = Group.objects.all()
        return render(request, "home.html", context={
            "header": "Peter Parker",
            "groups": groups
        })


class GroupView(TemplateView):
    template_name = "group.html"

    def get_context_data(self, **kwargs):
        group_id = kwargs["group_id"]
        group = Group.objects.get(id=group_id)

        logger.info(f"Пользователь посмотрел группу '{group.name}'")

        return {
            'group': group
        }


class AddGroupView(TemplateView):
    template_name = "add_group.html"

    def dispatch(self, request, *args, **kwargs):
        form = GroupForm
        if request.method == "POST":
            form = GroupForm(request.POST)

            if form.is_valid():
                form.save()
                # self.notify_about_new_group(request, form.instance)
                messages.success(request, "Группа успешно создана")
                messages.error(request, "Проверьте правильность введенных данных")

                logger.info(f"Пользователь создал группу '{form.instance.name}'")

                return redirect("/")
        return render(request, self.template_name, {"form": form})

    def notify_about_new_group(self, request, group):
        message = "На вашем сайте создана новая группа\n\n" \
                  f"Группа '{group.name}'\n\nВаш сайт."

        message_html = render_to_string("mail/new_group.html", {"group": group}, request)

        send_mail("Создана новая группа", message, settings.DEFAULT_FROM_EMAIL,
                  ['kubat.u@mail.ru', 'usubalievich@gmail.com', 'a.usubaliev08@gmail.com'], False, html_message=message_html)


class EditGroupView(TemplateView):
    template_name = "add_group.html"

    def dispatch(self, request, *args, **kwargs):
        group_id = kwargs["group_id"]
        group = Group.objects.get(id=group_id)

        form = GroupForm(instance=group)
        if request.method == "POST":
            form = GroupForm(request.POST, instance=group)
            if form.is_valid():
                form.save()
                messages.success(request, "Группа успешно изменена.")
                return redirect(reverse("group", kwargs={"group_id": group_id}))

        return render(request, self.template_name, {"form": form})


class AddStudentView(TemplateView):
    template_name = "add_student.html"

    def dispatch(self, request, *args, **kwargs):
        form = StudentForm

        group_id = kwargs["group_id"]
        group = Group.objects.get(id=group_id)

        if request.method == "POST":
            form = StudentForm(request.POST)

            if form.is_valid():
                form.instance.group = group
                form.save()
                messages.success(request, "Студент успешно создана")
                return redirect("/")
        return render(request, self.template_name, {"form": form, "group": group})


class AboutView(TemplateView):
    template_name = "about.html"

    def get_context_data(self, **kwargs):
        return {
            "header": "About page"
        }

class ExamplesView(TemplateView):
    template_name = "examples.html"

    def get_context_data(self, **kwargs):
        return {
            "header": "Enter your data"
        }


class TeachersView(TemplateView):
    template_name = "teacher.html"

    def get_context_data(self, **kwargs):
        teachers = Teacher.objects.all()
        return {
            'teachers': teachers
        }


class AddTeacherView(TemplateView):
    template_name = "add_teacher.html"

    def dispatch(self, request, *args, **kwargs):
        form = TeacherForm

        # teacher_id = kwargs["teacher.id"]
        # teacher = Teacher.objects.get(id=teacher_id)

        if request.method == "POST":
            form = TeacherForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, "Учитель успешно создан")
                return redirect("/teachers")
        return render(request, self.template_name, {"form": form})


class FeedbackView(TemplateView):
    template_name = "forms/feedback.html"

    def dispatch(self, request, *args, **kwargs):
        form = FeedbackForm
        return render(request, self.template_name, {"form": form})