from django.db import models
from django.utils.translation import ugettext_lazy as _
# from django.utils.translation import gettext as _



class Group(models.Model):
    STREAMS = [
        ('it', _('Информационныйе технологии')),
        ('philosophy', _('Философия')),
        ('ux/ui', _('UX/UI')),
        ('psychology', _('Психология'))
    ]

    name = models.CharField(verbose_name=_("Название"), max_length=255)
    stream = models.CharField(verbose_name=_("Напрвления"), max_length=255, choices=STREAMS)
    course = models.IntegerField(verbose_name=_("Курс"), default=1)

    class Meta:
        verbose_name = _("Группа")
        verbose_name_plural = _("Группы")

    def __str__(self):
        return self.name


class Student(models.Model):
    name = models.CharField(verbose_name=_("ФИО"), max_length=255)
    group = models.ForeignKey(Group, verbose_name=_("Группа"), on_delete=models.CASCADE, related_name="students")
    birthday = models.DateField(verbose_name=_("Дата рождения"), null=True, blank=True)
    email = models.EmailField(verbose_name=_("Почта"), null=True, blank=True)
    drop_out = models.BooleanField(verbose_name=_("Отчислен"), default=False)

    class Meta:
        verbose_name = _("Студент")
        verbose_name_plural = _("Студенты")

    def __str__(self):
        return self.name


class Teacher(models.Model):
    name = models.CharField(max_length=100, verbose_name=_("ФИО преподователя"))
    subject = models.CharField(max_length=50, verbose_name=_("Предмет"))
    position = models.CharField(max_length=70, verbose_name=_("Должность"), blank=True, null=True)
    group = models.ForeignKey(Group, verbose_name=_("Группа"), on_delete=models.CASCADE, related_name="groups")
    hired_date = models.DateField()

    class Meta:
        verbose_name = _("Учитель")
        verbose_name_plural = _("Учителя")

    def __str__(self):
        return self.name