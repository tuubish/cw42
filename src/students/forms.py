from django import forms
from django.contrib.auth.forms import UserCreationForm, UsernameField
from django.contrib.auth.models import User
from django.forms import ModelForm, Form

from students.models import Group, Student, Teacher


class GroupForm(ModelForm):
    class Meta:
        model = Group
        exclude = []
        # fields = ["name", "course", "stream"]


class StudentForm(ModelForm):
    class Meta:
        model = Student
        exclude = ['groups', 'drop_out']


class TeacherForm(ModelForm):
    class Meta:
        model = Teacher
        exclude = []


class FeedbackForm(Form):
    name = forms.CharField(label="Ваше имя", required=False)
    email = forms.EmailField(label="Ваш email")
    text = forms.CharField(label="Обратная связь", widget=forms.Textarea())


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'is_staff')
        field_classes = {"username": UsernameField}