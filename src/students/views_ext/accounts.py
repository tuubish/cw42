from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView

from students.forms import RegisterForm


class ProfileView(View):

    def dispatch(self, request, *args, **kwargs):

        return render(request, template_name="registration/profile.html" )


class LogoutView(View):
    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return redirect("/")


class RegisterView(TemplateView):
    template_name = "registration/register.html"

    def get_context_data(self, **kwargs):
        form = UserCreationForm()
        return {
            "form": form
        }

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(reverse("home_page"))

        form = RegisterForm()

        if request.method == "POST":
            form = RegisterForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, "Вы успешно зарегистрировались. Теперь войдите в систему")
                return redirect(reverse("login"))
        return render(request, self.template_name, {"form": form})