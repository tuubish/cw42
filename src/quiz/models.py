from django.db import models

# Create your models here.

class Quiz(models.Model):
    name = models.CharField(verbose_name="Название", max_length=255)
    time_in_minutes = models.IntegerField(verbose_name="Время в минутах")
    created_date = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True)
    active = models.BooleanField(default=False, verbose_name="Активен")

    class Meta:
        verbose_name = "Тест"
        verbose_name_plural = "Тесты"