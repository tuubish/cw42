import random

from django.core.management import BaseCommand

from quiz import Quiz


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        self.stdout.write("Команда запущена")

        number = random.randint(1, 1000)
        quiz = Quiz.objects.create(name=f"Тест #{number}", time_in_minutes=30)

        self.stdout.write(f"Тест {quiz.name} успешно создан")