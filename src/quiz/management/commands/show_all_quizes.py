from django.core.management import BaseCommand

from quiz import Quiz


class Command(BaseCommand):
    def handle(self, *args, **options):
        quizes = Quiz.objects.all()

        self.stdout.write(f"Всего {quizes.count()} тестов")

        for quiz in quizes:
            self.stdout.write(quiz.name)